#!/usr/bin/env bash

set -e
set -u

FILE_PATH="$1"                                   # le mp4 de la conf

USERNAME="jdll@jdll.org"                         # A renseigner
PASSWORD="xxxxxx"                                # A renseigner
API_PATH="https://www.videos-libr.es/api/v1"     # A renseigner
CHANNEL_ID="8"                                   # orga_jdll
# CHANNEL_ID="3"                                 # jdll
LICENCE="5"                                      # CC-NC-BY-SA
CATEGORY="15"                                    # Science & Technology
PRIVACY="3"                                      # Private
NAME="${FILE_PATH%.*}"
FILE_CAPTION_PATH="${NAME}".srt                  # Le fichier de soustitre
FILE_THUMB_PATH="${NAME}.png"                    # le png de miniature
NAME="${NAME#*/}"

## AUTH
client_id=$(curl -Ls "$API_PATH/oauth-clients/local" | jq -r ".client_id")
client_secret=$(curl -Ls "$API_PATH/oauth-clients/local" | jq -r ".client_secret")
token=$(curl -fs "$API_PATH/users/token" \
  --data client_id="$client_id" \
  --data client_secret="$client_secret" \
  --data grant_type=password \
  --data response_type=code \
  --data username="$USERNAME" \
  --data password="$PASSWORD" \
  | jq -r ".access_token")

## VIDEO UPLOAD
echo "Uploading video"
video_uuid=$(curl -Lf "$API_PATH/videos/upload" \
  -H "Authorization: Bearer $token" \
  --max-time 6000 \
  --form videofile=@"$FILE_PATH" \
  --form channelId=$CHANNEL_ID \
  --form name="$NAME" \
  --form category="${CATEGORY}" \
  --form licence=${LICENCE} \
  --form description="MY_VIDEO_DESCRIPTION" \
  --form language=fr \
  --form privacy=${PRIVACY} \
  --form waitTranscoding=1 \
  --form thumbnailfile=@"$FILE_THUMB_PATH" \
  --form tags="JDLL" \
  --form tags="JDLL2023" \
  | jq -r ".video.shortUUID")

## CAPTION UPLOAD
echo "Uploading captions"
curl -Lf -X PUT "$API_PATH/videos/$video_uuid/captions/fr" \
  -F "captionfile=@$FILE_CAPTION_PATH" \
  -H "Authorization:Bearer $token" \
  -H "Accept:application/json"

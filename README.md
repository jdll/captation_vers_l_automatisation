# JdLL automatisation de publication des vidéos
L'idée est d'arriver à automatiser au plus le traitement des vidéos des confs, depuis le fichier vidéo + audio (bien synchro) jusqu'à la publication sur Peertube.
Ici on voit comment on lance une reconnaissance vocale (TTS) pour obtenir des sous-titres et comment on publie sur peertube par script.

## Dépendances
    sudo apt update && sudo apt upgrade
    sudo apt install jq pip
    pip install -U openai-whisper

## La transcription avec whisper
On lance la transcription sur les .mp4, avec :
```
for vid in *.mp4; do whisper "$vid" --language French --model large --output_format srt; done
```
On obtient des fichiers .srt pour chaque conférence.

**Attention c'est long !**
Chez moi avec 10 vcores je compte au moins **4 fois et demi la durée de la conférence** ! Oui, 1h de conférence demande 4H30 de temps de calcul...
Pour des durées plus courtes des qualités plus faibles sont possibles, mais après plusieurs essais je les ai trouvées trop médiocres et irrecevables.
Un truc louche est qu'en plein boulot mon proc reste à 60% de charge. Je ne sais pas pourquoi.
Avec une carte graphique, ajouter l'option ```--device cuda```

Edit : Test réalisé avec une GeForce GTX 1660 Super de 6Go. Elle ne dispose pas suffisemment de mémoire pour whisper en mode 'large', j'ai donc fait le test en mode 'medium' qui ne nécessite 'que' 5Go de RAM. Waow, **38 minutes** de traitement pour une conf de 51 minutes. Même si la base n'est pas très comparable : ça vaut le coup.

## La publication sur Peertube

Ce script bash fait l'upload sur Peertube de la vidéo avec ses metas et les sous-titres.
Ce sera cool de le combiner avec le js pour récupérer du pretalx les metas qui vont bien : titre, orateur, résumé, description, et autres informations fixes par édition.

S'utilise de la sorte :
```$ bash lenomduscript.bash chemin/fichiervideo.mp4 autrechemin/miniature.jpg```

Le fichier des sous-titres **doit** être : chemin/fichiervideo.srt
*Mais peut-être qu'on pourra simplifier en mettant le même chemin et nom de fichier, seule l'extension diffère : chemin/fichier.mp4, chemin/fichier.png, chemin/fichier.srt*

## À faire :

- vérifier que je n'ai pas fait trop de bêtises
- faire le lien avec nos données issues du pretalx, ex:
    - titre peertube = nom de la conf + nom orateur + "JdLL2023"
    - description peertube = résumé + description
    - le fichier de miniature
- avoir un log des erreurs
- automatiser à une cadense choisie la publication des vidéos et des annonces (envoi de toots).
